This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

To create a production build, use npm run build.

To run localy run npm start

## PWA

This project includes a service worker so that the app loads from local cache on future visits. It also contains a manifest
file.


## To do next

* Add sass loader for webpack. Having sass will reduce the duplicate code, make the css code easier to maintain.
* Review the components template
* Integrate firebase in the app for login and add/remove/edit invoice. When selecting a bank load only invoice for that bank.


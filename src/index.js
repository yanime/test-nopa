import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import firebase from 'firebase';

// Initialize Firebase
const config = {
    apiKey: "AIzaSyBm9ho2CnpQgT2LANFjsxZtdwzcm_8e3wA",
    authDomain: "bank-app-fa6f8.firebaseapp.com",
    databaseURL: "https://bank-app-fa6f8.firebaseio.com",
    projectId: "bank-app-fa6f8",
    storageBucket: "bank-app-fa6f8.appspot.com",
    messagingSenderId: "951166104190"
};
firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();

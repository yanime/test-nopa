import React, { Component } from 'react';
import './BankAccounts.css';
import barcklaysImg from '../../assets/Barclays.png';
import logoHSBC from '../../assets/LogoHSBC.png';
import logoLloyds from '../../assets/LogoLloyds.png';
import logoNatwest from '../../assets/LogoNatwest.png';
import logoSantander from '../../assets/LogoSantander.png';
import logoTsb from '../../assets/LogoTSB.png';
import {Link} from 'react-router-dom'

class BankAccounts extends Component {
    render() {
        return (
            <div className="bank-accounts">
                <div className="bank-accounts__wrapper">
                    <div className="bank-accounts__text">
                        <h2>There’s no such things as “one size fits all” finance.</h2>
                        <p>Track of all your payments by connecting as many
                            bank accounts as you’d like to your Nopa account and get updates your balance instantly.
                            Plus it’s free.</p>
                    </div>
                </div>
                <div className="bank-accounts__wrapper">
                    <div className="bank-accounts__banks">
                        <div className="bank-accounts__row">
                            <div className="bank-accounts__item">
                                <img src={barcklaysImg} className="bank-accounts__image" alt="bank-logo" />
                            </div>
                            <div className="bank-accounts__item">
                                <img src={logoHSBC} className="bank-accounts__image" alt="bank-logo" />
                            </div>
                        </div>
                        <div className="bank-accounts__row">
                            <div className="bank-accounts__item">
                                <img src={logoNatwest} className="bank-accounts__image" alt="bank-logo" />
                            </div>
                            <div className="bank-accounts__item">
                                <img src={logoTsb} className="bank-accounts__image" alt="bank-logo" />
                            </div>
                        </div>
                        <div className="bank-accounts__row">
                            <div className="bank-accounts__item">
                                <img src={logoSantander} className="bank-accounts__image" alt="bank-logo" />
                            </div>
                            <div className="bank-accounts__item">
                                <img src={logoLloyds} className="bank-accounts__image" alt="bank-logo" />
                            </div>
                        </div>
                        <div className="bank-accounts__row">
                            <Link to="/loginBank">Log in & connect</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default BankAccounts;

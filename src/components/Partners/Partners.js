import React, { Component } from 'react';
import './Partners.css';
import airbnbImg from './Airbnb.png';
import metroImg from './Metro.png';
import paritiImg from './Pariti.png';
import unshackledImg from './Unshackled.png';

class Partners extends Component {
    render() {
        return (
            <div className="partners">
                <div className="partners_item">
                    <span>Our partners:</span>
                </div>
                <div className="partners_item">
                    <img src={airbnbImg} alt="airbnb" />
                </div>
                <div className="partners_item">
                    <img src={metroImg} alt="mentro" />
                </div>
                <div className="partners_item">
                    <img src={paritiImg} alt="pariti" />
                </div>
                <div className="partners_item">
                    <img src={unshackledImg} alt="unshackled" />
                </div>
            </div>
        )
    }
}

export default Partners;
  
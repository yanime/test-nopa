import React, { Component } from 'react';
import Button from '../Button/Button';
import './Header.css';
import logo from '../logo.svg';
import {Link} from 'react-router-dom'

class Header extends Component {
    handleClick(){
        console.log('click');
    };
    renderLogo(){
        if(this.props.displayLogo){
            return <Link to="/"><img src={logo} className="App-logo" alt="logo" /></Link>
        }
        return null;
    };
    render() {
        return (
      <div className="header">
          <div className="header__logo">
          {this.renderLogo()}
          </div>
          <div className="header__button">
              <Button label="Log In" handleClick={this.handleClick}></Button>
          </div>
      </div>
    )
  }
}

export default Header;
  
import React, { Component } from 'react';
import './PaymentInfo.css';
import logo from '../logo.svg';
import {Link} from 'react-router-dom'

class PaymentInfo extends Component {
    handleClick(){
        console.log('click');
    };
    render() {
        return (
            <div className="payment-info">
                <div className="payment-info__row">
                    <img src={logo} className="payment-info__image" alt="logo" />
                </div>
                <div className="payment-info__row">
                    <h2>Your finances, in one place</h2>
                </div>
                <div className="payment-info__row">
                    <p>Track of all your payments by connecting as many bank accounts as you’d like to your Nopa account
                        and get updates your balance instantly.</p>
                </div>
                <div className="payment-info__row">
                    <Link to="/chooseBank">Get Started</Link>
                </div>
            </div>
        )
    }
}

export default PaymentInfo;

import React, { Component } from 'react';
import Header from '../../Header/Header';
import Partners from '../../Partners/Partners';
import Invoice from '../../Invoice/Invoice';
import About from '../../About/About';
import './style.css';

class Statement extends Component {
    render() {
        return (
            <div className="choose-bank">
                <Header displayLogo={true}></Header>
                <Invoice></Invoice>
                <Partners></Partners>
                <About></About>
            </div>
        );
    }
}

export default Statement;
import React, { Component } from 'react';
import Header from '../../Header/Header';
import Partners from '../../Partners/Partners';
import BankForm from '../../BankForm/BankForm';
import About from '../../About/About';
import './style.css';
class App extends Component {
    render() {
        return (
            <div className="login-bank">
                <Header displayLogo={true}></Header>
                <BankForm></BankForm>
                <Partners></Partners>
                <About></About>
            </div>
        );
    }
}

export default App;

import React, { Component } from 'react';
import Header from '../../Header/Header';
import BankAccounts from '../../BankAccounts/BankAccounts';
import Partners from '../../Partners/Partners';
import About from '../../About/About';
import './style.css';

class ChooseBank extends Component {
    render() {
        return (
            <div className="choose-bank">
                <Header displayLogo={true}></Header>
                <BankAccounts></BankAccounts>
                <Partners></Partners>
                <About></About>
            </div>
        );
    }
}

export default ChooseBank;

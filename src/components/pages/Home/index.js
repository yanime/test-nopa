import React, { Component } from 'react';
import Header from '../../Header/Header';
import PaymentInfo from '../../PaymentInfo/PaymentInfo';
import FinanceInfo from '../../FinanceInfo/FinanceInfo';
import Partners from '../../Partners/Partners';
import About from '../../About/About';
import './style.css';

class Home extends Component {
    render() {
        return (
            <div className="App">
                <Header displayLogo={false}></Header>
                <PaymentInfo></PaymentInfo>
                <FinanceInfo></FinanceInfo>
                <Partners></Partners>
                <About></About>
            </div>
        );
    }
}

export default Home;

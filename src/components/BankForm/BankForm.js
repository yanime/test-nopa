import React, { Component } from 'react';
import './BankForm.css';
import {Redirect} from 'react-router-dom'
import Button from '../Button/Button';

function validate(surname, accountNumber, passcode) {
    return {
        surname: surname.length === 0,
        accountNumber: accountNumber.length === 0,
        passcode: passcode.length === 0
    };
}

class BankForm extends Component {
    constructor() {
        super();
        this.state = {
            surname: '',
            sortCode: '',
            accountNumber: '',
            passcode: '',
            momorableWord: '',
            errors: {},
            redirect: false
        };
    }

    handleClick(){
        const errors = validate(this.state.surname, this.state.accountNumber, this.state.passcode);
        const isDisabled = Object.keys(errors).some(x => errors[x]);
        this.setState({ errors: errors });
        if(!isDisabled){
            this.setState({ redirect: true });
        }
    };
    handleSurnameChange = (evt) => {
        this.setState({ surname: evt.target.value });
    };
    handleSortCodeChange = (evt) => {
        this.setState({ sortCode: evt.target.value });
    };
    handleAccountNumberChange = (evt) => {
        this.setState({ accountNumber: evt.target.value });
    };
    handlePasscodeChange = (evt) => {
        this.setState({ passcode: evt.target.value });
    };
    handlememorableWordChange = (evt) => {
        this.setState({ momorableWord: evt.target.value });
    };
    render() {
        if (this.state.redirect) {
            return <Redirect to='/statement'/>;
        }
        return (
            <div className="bank-form">
                <div className="bank-form__wrapper">
                    <div className="bank-form__text">
                        <h2>There’s no such things as “one size fits all” finance.</h2>
                        <p>Track of all your payments by connecting as many
                            bank accounts as you’d like to your Nopa account and get updates your balance instantly.
                            Plus it’s free.</p>
                    </div>
                </div>
                <div className="bank-form__wrapper">
                    <div className="bank-form__column">
                        <div className="bank-form__item">
                            <input placeholder="Surname"
                                value={this.state.surname}
                                onChange={this.handleSurnameChange}
                                className={this.state.errors.surname ? "error" : ""}
                            />
                        </div>
                        <div className="bank-form__item">
                            <input placeholder="Sort Code"
                                value={this.state.sortCode}
                                onChange={this.handleSortCodeChange}/>
                        </div>
                        <div className="bank-form__item">
                            <input placeholder="Account Number"
                                value={this.state.accountNumber}
                                onChange={this.handleAccountNumberChange}
                                className={this.state.errors.accountNumber ? "error" : ""}
                            />
                        </div>
                        <div className="bank-form__item">
                            <input placeholder="Passcode"
                                value={this.state.passcode}
                                onChange={this.handlePasscodeChange}
                                className={this.state.errors.passcode ? "error" : ""}
                            />
                        </div>
                        <div className="bank-form__item">
                            <input placeholder="Memorable word"
                                value={this.state.momorableWord}
                                onChange={this.handlememorableWordChange}/>
                        </div>
                        <div className="bank-form__item">
                            <Button label="Log In & connect" handleClick={this.handleClick.bind(this)}></Button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default BankForm;

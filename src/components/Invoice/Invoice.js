import React, { Component } from 'react';
import './Invoice.css';

class Invoice extends Component {
    render() {
        return (
            <div className="invoice">
                <div className="invoice__wrapper">
                    <div className="invoice__text">
                        <h2>Your transactions for the last 30 days</h2>
                    </div>
                </div>
                <div className="invoice__wrapper">
                    <div className="invoice__table">
                        <div className="invoice__row grey">
                            <span>Today</span>
                        </div>
                        <div className="invoice__row">
                            <span>PAYPAL ZARA</span>
                            <div className="invoice_column">
                                <span>-</span>
                                <span>$34.33</span>
                            </div>
                        </div>
                        <div className="invoice__row">
                            <span>HOUSE OF FRASER</span>
                            <div className="invoice_column">
                                <span>-</span>
                                <span>$35.98</span>
                            </div>
                        </div>
                        <div className="invoice__row">
                            <span>TESC</span>
                            <div className="invoice_column">
                                <span>-</span>
                                <span>$34.33</span>
                            </div>
                        </div>
                        <div className="invoice__row grey">
                            <span>22 November 2016</span>
                        </div>
                        <div className="invoice__row">
                            <span>John Lewis</span>
                            <div className="invoice_column">
                                <span>-</span>
                                <span>$34.33</span>
                            </div>
                        </div>
                        <div className="invoice__row">
                            <span>Zara</span>
                            <div className="invoice_column">
                                <span>-</span>
                                <span>$35.98</span>
                            </div>
                        </div>
                        <div className="invoice__row">
                            <span>TESC</span>
                            <div className="invoice_column">
                                <span>-</span>
                                <span>$34.33</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Invoice;
  
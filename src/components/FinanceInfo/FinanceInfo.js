import React, { Component } from 'react';
import './FinanceInfo.css';
import image from './Shapes.svg';

class FinanceInfo extends Component {
    render() {
        return (
            <div className="finance-info">
                <div className="finance-info__container">
                    <div className="finance-info__text">
                        <h2>There’s no such things as “one size fits all” finance.</h2>
                        <p>We were founded to make money simple and fair, for everyone: whether you’re looking for a loan,
                            or to get better rewards for your investments.</p>
                    </div>
                    <div className="finance-info__image">
                        <img src={image} className="finance-info__image" alt="shapes-logo" />
                    </div>
                </div>
            </div>
        )
    }
}

export default FinanceInfo;

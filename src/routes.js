// src/routes.js
import React from 'react';
import {
    BrowserRouter as Router,
    Route
    } from 'react-router-dom'
import Home from './components/pages/Home';
import ChooseBank from './components/pages/ChooseBank';
import LoginBank from './components/pages/LoginBank';
import Statement from './components/pages/Statement';

const Routes = (props) => (
    <Router>
        <div>
            <Route exact path="/" component={Home}/>
            <Route exact path="/chooseBank" component={ChooseBank}/>
            <Route exact path="/loginBank" component={LoginBank}/>
            <Route exact path="/statement" component={Statement}/>
        </div>
    </Router>
);

export default Routes;